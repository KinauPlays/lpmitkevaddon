package de.lpmitkev.kinau.lasertag;

import net.minecraft.util.Vec3;
import net.minecraftforge.client.event.DrawBlockHighlightEvent;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import org.lwjgl.opengl.GL11;

import java.util.ArrayList;
import java.util.List;

public class DrawBlockHighlightListener {

    @SubscribeEvent
    public void onDrawBlockHighlightEvent(DrawBlockHighlightEvent event){
        GL11.glPushMatrix();
        GL11.glPushAttrib(GL11.GL_ENABLE_BIT);
        double d0 = event.player.prevPosX + (event.player.posX - event.player.prevPosX) * (double)event.partialTicks;
        double d1 = event.player.prevPosY + (event.player.posY - event.player.prevPosY) * (double)event.partialTicks;
        double d2 = event.player.prevPosZ + (event.player.posZ - event.player.prevPosZ) * (double)event.partialTicks;
        Vec3 pos = new Vec3(d0, d1, d2);
        GL11.glTranslated(-pos.xCoord, -pos.yCoord, -pos.zCoord);
        GL11.glDisable(GL11.GL_LIGHTING);
        GL11.glDisable(GL11.GL_TEXTURE_2D);

        List<Laser> toRemove = new ArrayList<Laser>();
        for(Laser laser : Laser.currentLasers) {
            laser.shoot();
            if(laser.getTicksLeft() <= 0)
                toRemove.add(laser);
        }

        Laser.currentLasers.removeAll(toRemove);
        toRemove.clear();

        GL11.glPopAttrib();
        GL11.glPopMatrix();
    }
}
