package de.lpmitkev.kinau.lasertag;

import de.lpmitkev.kinau.utils.GLUtils;
import net.minecraft.util.Vec3;
import org.lwjgl.util.Color;

import java.util.ArrayList;
import java.util.List;

public class Laser {

    public static List<Laser> currentLasers = new ArrayList<Laser>();

    private Vec3 start, stop;
    private Color color;
    private long ticksLeft;

    public Laser(Vec3 start, Vec3 end, Color color) {
        this.start = start;
        this.stop = end;
        this.color = color;
        this.ticksLeft = 4000;
        currentLasers.add(this);
    }

    public Vec3 getStart() {
        return start;
    }

    public Vec3 getStop() {
        return stop;
    }

    public Color getColor() {
        return color;
    }

    public long getTicksLeft() {
        return ticksLeft;
    }

    public void shoot() {
        if(start == null || stop == null)
            return;
        GLUtils.drawLine(start, stop, color);
        ticksLeft--;
    }
}
