package de.lpmitkev.kinau.utils;

import net.minecraft.util.Vec3;
import org.lwjgl.opengl.GL11;
import org.lwjgl.util.Color;

public class GLUtils {

    public static void drawLine(Vec3 blockA, Vec3 blockB, Color color) {
        GL11.glColor3f(color.getRed() / 255.0F, color.getGreen() / 255.0F, color.getBlue() / 255.0F);

        GL11.glLineWidth(1);

        GL11.glBegin(GL11.GL_LINE_STRIP);
        GL11.glVertex3d(blockA.xCoord, blockA.yCoord, blockA.zCoord);
        GL11.glVertex3d(blockB.xCoord, blockB.yCoord, blockB.zCoord);
        GL11.glEnd();
    }
}
