package de.lpmitkev.kinau;

import de.lpmitkev.kinau.lasertag.DrawBlockHighlightListener;
import de.lpmitkev.kinau.server.LPmitKevServer;
import net.labymod.api.LabyModAddon;
import net.labymod.settings.elements.BooleanElement;
import net.labymod.settings.elements.ControlElement;
import net.labymod.settings.elements.SettingsElement;
import net.labymod.utils.Material;

import java.util.List;

public class LPmitKevAddon extends LabyModAddon {

    private static LPmitKevAddon instance;
    private LPmitKevServer lPmitKevServer;
    private boolean laserActive, displayServerState;

    @Override
    public void onEnable() {
        instance = this;
        lPmitKevServer = new LPmitKevServer();
        getApi().registerServerSupport(getInstance(), lPmitKevServer);
        getApi().registerForgeListener(new DrawBlockHighlightListener());
    }

    @Override
    public void onDisable() {

    }

    @Override
    public void loadConfig() {
        this.laserActive = !getConfig().has("useNewLaser") || getConfig().get("useNewLaser").getAsBoolean(); // <- default value 'true'
        this.displayServerState = !getConfig().has("displayServerState") || getConfig().get("displayServerState").getAsBoolean(); // <- default value 'true'
    }

    @Override
    protected void fillSettings(List<SettingsElement> list) {
        list.add(new BooleanElement("Enable alternative lasers (LaserTag)", this, new ControlElement.IconData(Material.BLAZE_ROD), "useNewLaser", laserActive));
        list.add(new BooleanElement("Shows which game you are playing", this, new ControlElement.IconData(Material.NAME_TAG), "displayServerState", displayServerState));
    }

    @Override
    public void saveConfig() {
        super.saveConfig();
        loadConfig();
        lPmitKevServer.update();
    }

    public static LPmitKevAddon getInstance() {
        return instance;
    }

    public boolean isDisplayServerStateActive() {
        return displayServerState;
    }

    public boolean isLaserActive() {
        return laserActive;
    }
}
